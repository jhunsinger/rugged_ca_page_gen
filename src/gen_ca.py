import ma_api
import time
import json
import re


def get_email_account_mapper():
    # Init closure vars
    email_account_map = {}
    domain_account_map = {}

    # Load maps
    pattern = re.compile('^\"(.*?)\" (.*)$')
    with open('account_email.txt') as f:
        for line in f:
            m = pattern.match(line)
            if not m:
                continue
            
            account_name, email = m.groups( (1, 2) )
            email_account_map[email] = account_name

            try:
                domain = email.split('@')[1]
                domain_account_map[domain] = account_name
            except IndexError:
                pass

    # Define mapper 
    def email_account_mapper(email):
        if email in email_account_map:
            return email_account_map[email]
        
        domain = email.split('@')[1]
        if domain in domain_account_map:
            return domain_account_map[domain]
        
        return None
    
    # Return mapper
    return email_account_mapper


def get_row_builder(email_account_mapper, account_name_overrides=None):
    # Load row template from disk
    with open('templates/cloud_access_row_template.html') as f:
        row_template = f.read()
    
    # Define row builder
    def row_builder(appliance):
        # All appliances should have these three fields
        appliance_id = appliance['appl_id']
        email        = appliance['email']
        port         = appliance['port']

        # Get account name if possible. Otherwise use appliance_id
        if account_name_overrides and appliance_id in account_name_overrides:
            account_name = account_name_overrides[appliance_id]
        elif 'accountName' in appliance:
            account_name = appliance['accountName']
        else:
            account_name = email_account_mapper(email)
            if not account_name:
                account_name = appliance_id
        
        # Get appliance type if possible. Otherwise use 'Unknown'.
        try:
            appliance_type = appliance['data']['software']['applianceType']
            if appliance_type.strip() == '':
                appliance_type = 'Unknown'
        except:
            appliance_type = 'Unknown'
        
        # Instence number is just the first 8 characters of the appliance_id now
        instance_num = appliance_id[:8]

        # Get the private IP if possible. Otherwise use '0.0.0.0'
        try:
            private_ip = appliance['data']['network']['internalIP']
        except:
            private_ip = '0.0.0.0'
        
        # Get SafeConnect version if possible. Otherwise use 'Unknown'.
        try:
            sc_version = appliance['data']['software']['scVersion']
        except:
            sc_version = 'Unknown'
        
        # Construct row
        row = row_template.replace(':ACCOUNT_NAME:', account_name)
        row = row.replace(':APPLIANCE_TYPE:', appliance_type)
        row = row.replace(':INSTANCE_NUM:', instance_num)
        row = row.replace(':PRIVATE_IP:', private_ip)
        row = row.replace(':VERSION:', sc_version)
        row = row.replace(':PORT:', str(port))
        row = row.replace(':PORT_1:', str(port+1))
        row = row.replace(':PORT_2:', str(port+2))
        row = row.replace(':PORT_3:', str(port+3))
        row = row.replace(':PORT_4:', str(port+4))

        return row
    
    return row_builder

def print_stats(appliance_data, email_account_mapper):
    # Get and print statistics
    missing_account_name = 0
    missing_account_and_email = 0
    email_lookup_failed = 0
    for appliance in appliance_data:
        if 'accountName' not in appliance:
            missing_account_name += 1
            if 'email' not in appliance:
                missing_account_and_email += 1
            else:
                if email_account_mapper(appliance['email']) is None:
                    email_lookup_failed += 1

    print('Total appliances:', len(appliance_data))
    print('Appliances missing account name:', missing_account_name)
    print('Appliances missing account and email:', missing_account_and_email)
    print('Email lookup failures:', email_lookup_failed)


def generate_ca_page(appliance_data, account_name_overrides, email_account_mapper, output_path):
    # Get function to build HTML table rows from appliance objects
    row_builder = get_row_builder(email_account_mapper, account_name_overrides)

    # Compile rows
    rows = []
    for appliance in appliance_data:
        try:
            rows.append( row_builder(appliance) )
        except KeyError:
            pass

    rows.sort()
    print('Got', len(rows), 'rows to write.')

    # Create the page
    with open(output_path, 'w') as out_file:
        with open('templates/cloud_access_bookends.html') as bookends_file:
            for line in bookends_file:
                if 'CONTENT HERE' in line:
                    for row in rows:
                        out_file.write(row + '\n')
                else:
                    out_file.write(line)
    print('Finished writing page.')

def appliance_name_generator(appliance, account_name_overrides, email_account_mapper):
    # All appliances should have these three fields
    appliance_id = appliance['appl_id']
    email        = appliance['email']
    port         = appliance['port']

    # Get account name if possible. Otherwise use appliance_id
    if account_name_overrides and appliance_id in account_name_overrides:
        account_name = account_name_overrides[appliance_id]
    elif 'accountName' in appliance:
        account_name = appliance['accountName']
    else:
        account_name = email_account_mapper(email)
        if not account_name:
            account_name = appliance_id
    
    account_name = account_name.lower().replace(' ', '-')
    
    # Get appliance type if possible. Otherwise use 'Unknown'.
    try:
        appliance_type = appliance['data']['software']['applianceType']
        if appliance_type.strip() == '':
            appliance_type = 'Unknown'
    except:
        appliance_type = 'Unknown'
    
    # Instence number is just the first 8 characters of the appliance_id now
    instance_num = appliance_id[:8]

    return f'{account_name}-{appliance_type}-{instance_num}'

def generate_ssh_config_file(appliance_data, account_name_overrides, email_account_mapper, output_path):
    host_port_pairs = []
    for appliance in appliance_data:
        try:
            host = appliance_name_generator(appliance, account_name_overrides, email_account_mapper)
            port = appliance['port']
            host_port_pairs.append( (host, port) )
        except KeyError: 
            continue
    
    host_port_pairs.sort(key=lambda h: h[0])
    with open(output_path, 'w') as f:
        for host, port in host_port_pairs:
            f.write(f'Host {host}\n')
            f.write( '    HostName localhost\n')
            f.write( '    User Management\n')
            f.write(f'    Port {port}\n')
            f.write( '    ProxyJump ca2\n\n')


if __name__ == '__main__':
    print('Generating fresh CA page.')

    # Get appliance data from MetaAccess
    print('Getting appliance data...', end='')
    stage = 'production'
    auth_token = ma_api.create_jwt(stage)
    appliance_data = ma_api.get_appliance_data(stage, auth_token)
    print('Done')
    
    # Write raw appliance data to web accessible file. This info is often useful.
    with open('www/appliance_data.json', 'w') as f:
        json.dump(appliance_data, f, indent='    ')
    
    # Load account_name overrides
    with open('appliance_account_overrides.json') as f:
        account_name_overrides = json.load(f)
    
    # Get email --> account name mapper
    email_account_mapper = get_email_account_mapper()

    generate_ca_page(appliance_data, account_name_overrides, email_account_mapper, 'www/cloud_access.html')
    generate_ssh_config_file(appliance_data, account_name_overrides, email_account_mapper, 'www/ssh_config')

