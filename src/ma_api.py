# common utilities for fetching data

import datetime
import http.client
import ssl
import json
import python_jwt
import jwcrypto

private_key_path = '/opt/ca_gen/jwt_signer.pem'

def get_jwt_private_key(stage):
	with open(private_key_path, 'rb') as f:
		return f.read()

def create_jwt(stage):
	private_key = jwcrypto.jwk.JWK.from_pem(get_jwt_private_key(stage))
	result = python_jwt.generate_jwt({}, private_key, "RS512", datetime.timedelta(minutes=1))
	return result

def get_base_url(stage):
	if stage == "testing":
		return "gears-testing.opswat.com"
	if stage == "staging":
		return "gears-staging.opswat.com"
	if stage == "beta":
		return "gears-beta.opswat.com"
	if stage == "production":
		return "gears.opswat.com"
	raise Exception("unrecognized stage: %s" % stage)

def make_https_request(method, host, path, headers=None, body=None):
	if host.startswith("http://"):
		raise Exception("expected https://")
	if host.startswith("https://"):
		host = host[len("https://"):]
	if path.startswith("/"):
		path = path[1:]
	connection = http.client.HTTPSConnection(host)
	if not headers:
		headers = {}
	request = connection.request(
		method,
		"/%s" % path,
		headers=headers,
		body=json.dumps(body) if body else None
	)
	response = connection.getresponse()
	response_body = response.read()
	if response.status < 200 or response.status > 299:
		log_str = host + path
		raise Exception("status code %s from %s" % (response.status, log_str))
	json_response = json.loads(str(response_body, "utf8"))
	# MA likes to respond with 200, but with an error code
	# docs indicate this should be in an "error" property, but experimentally this seems to come back as "code"
	# https://opswat.atlassian.net/wiki/spaces/GEAR/pages/149113249/Common+Error+Code
	if "code" in json_response:
		raise Exception("error code %s" % json_response["code"])
	return json_response

def get_appliance_data(stage, auth_token):
	page_size = 500
	def get_page(page):
		return make_https_request(
			method="GET",
			host=get_base_url(stage),
			path="/gears/api/sc/v1/appliance?size=%s&page=%s" % (page_size, page),
			headers={
				"Authorization": "Bearer %s" % auth_token
			}
		)["items"]
	page = 0
	results = []
	while True:
		page_data = get_page(page)
		if len(page_data) == 0:
			break
		results.extend(page_data)
		page+=1
	return results
