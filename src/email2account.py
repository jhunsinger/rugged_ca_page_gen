import ma_api
import re

def get_email_account_mapper():
    # Init closure vars
    email_account_map = {}
    domain_account_map = {}

    # Load maps
    pattern = re.compile('^\"(.*?)\" (.*)$')
    with open('account_email.txt') as f:
        for line in f:
            m = pattern.match(line)
            if not m:
                continue
            
            account_name, email = m.groups( (1, 2) )
            email_account_map[email] = account_name

            try:
                domain = email.split('@')[1]
                domain_account_map[domain] = account_name
            except IndexError:
                pass

    # Define mapper 
    def email_account_mapper(email):
        if email in email_account_map:
            return email_account_map[email]
        
        domain = email.split('@')[1]
        if domain in domain_account_map:
            return domain_account_map[domain]
        
        return None
    
    # Return mapper
    return email_account_mapper

# Get appliance data from MetaAccess
stage = 'production'
auth_token = ma_api.create_jwt(stage)
appliance_data = ma_api.get_appliance_data(stage, auth_token)

# Get email --> account name mapper
email_account_mapper = get_email_account_mapper()

# Print emails with accounts
lines = []
with open('/tmp/emails.txt') as f:
    for email in f:
        email = email.strip()
        account = email_account_mapper(email)
        domain = email.split('@')[1]
        
        versions = []
        for appliance in appliance_data:
            try:
                if appliance['email'].split('@')[1] == domain:
                    versions.append( appliance['data']['software']['scVersion'] )
            except:
                pass
        
        versions.sort()

        lines.append( '{:<31} {:<45} {}'.format(email, account, versions[-1]) )

lines.sort(key=lambda l:l.split()[-1], reverse=True)
for line in lines:
    print(line)
