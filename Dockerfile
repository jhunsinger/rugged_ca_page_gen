FROM nginx

# Install Packages
RUN ["apt", "update"]
RUN ["apt", "install", "-y", "python3"]
RUN ["apt", "install", "-y", "python3-pip"]
RUN ["apt", "install", "-y", "cron"]

# Install required Python libs
RUN ["python3", "-m", "pip", "install", "python_jwt"]

# Put our page generator script in place
WORKDIR /opt/ca_gen
ADD src .
COPY jwt_signer.pem .

# Set cron to run our script
COPY gen_ca.cron /etc/cron.d/
RUN ["crontab", "/etc/cron.d/gen_ca.cron"]

# Overwrite default nginx config with our custom one
COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY emails.txt /tmp/

# Start cron and nginx
CMD cron && nginx && pwd && python3 ./gen_ca.py > /var/log/ca_gen.log && tail -F /var/log/ca_gen.log
